//
//  Game.swift
//  FortisTDDTest
//
//  Created by Malek Mansour on 11/10/2020.
//

import Foundation

class Game {

    private var scores: [Int] = Array(repeating: 0, count: 21)
    private var currentRoll = 0

    func roll(_ pins: Int) {
        scores[currentRoll] = pins
        currentRoll += 1
    }

    func score() -> Int {
        var roll = 0
        var spareBonus = 0
        var strikeBonus = 0
        for _ in 1...9 {
            if isStrike(for: scores[roll]) {
                strikeBonus += scores[roll + 1] + scores[roll + 2]
                roll += 1
            } else if isSpare(for: scores[roll], then: scores[roll + 1]) {
                spareBonus += scores[roll + 2]
                roll += 2
            } else {
                roll += 2
            }
        }
        return scores.reduce(0, +) + spareBonus + strikeBonus
    }

    private func isSpare(for firstPin: Int, then secondPin: Int) -> Bool {
        return firstPin + secondPin == 10 ? true : false
    }

    private func isStrike(for firstPin: Int) -> Bool {
        return firstPin == 10 ? true : false
    }

}
