//
//  FortisTDDTestTests.swift
//  FortisTDDTestTests
//
//  Created by Malek Mansour on 11/10/2020.
//

import XCTest
@testable import FortisTDDTest

class FortisTDDTestTests: XCTestCase {

    var game: Game! = Game()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    private func roll(_ seriesOfPins: [Int]) {
        for pins in seriesOfPins {
            game.roll(pins)
        }
    }

    private func roll(knockPins: Int, times: Int) throws {
        for _ in 1...times {
            game.roll(knockPins)
        }
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testRealGame() {
        game.roll(1)
        game.roll(4)
        game.roll(4)
        game.roll(5)
        game.roll(6)
        game.roll(4)
        game.roll(5)
        game.roll(5)
        game.roll(10)
        game.roll(0)
        game.roll(1)
        game.roll(7)
        game.roll(3)
        game.roll(6)
        game.roll(4)
        game.roll(10)
        game.roll(2)
        game.roll(8)
        game.roll(6)
        XCTAssertEqual(game.score(), 133)
    }
    
    func testGutterGame() throws {
        try roll(knockPins: 0, times: 20)
        XCTAssertEqual(game.score(), 0)
    }
    
    func testAllTwoPins() throws {
        try roll(knockPins: 2, times: 20)
        XCTAssertEqual(game.score(), 40)
    }
    
    func testSpare() throws {
        game.roll(1)
        game.roll(4)
        game.roll(5)
        game.roll(5)
        game.roll(6)
        game.roll(0)
        XCTAssertEqual(game.score(), 27)
    }

    func testAllFivePins() throws {
        try roll(knockPins: 5, times: 21)
        XCTAssertEqual(game.score(), 150)
    }

    func testOneStrike() throws {
        game.roll(10)
        game.roll(4)
        game.roll(5)
        game.roll(4)
        XCTAssertEqual(game.score(), 32)
    }

    func testConsecutiveStrikes() throws {
        roll([10, 10, 10, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        XCTAssertEqual(game.score(), 81)
    }

    func testStrikeInLastFrame() {
        roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 7, 1])
        XCTAssertEqual(game.score(), 18)
    }

    func testStrikesWithTwoRollBonus() {
        roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 10])
        XCTAssertEqual(game.score(), 30)
    }

    func testStrikeAfterSpareInLastFrame() {
         roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 3, 10])
        XCTAssertEqual(game.score(), 20)
    }

    func testAllStrikes() {
         roll([10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10])
        XCTAssertEqual(game.score(), 300)
    }

    func testAllZeros() {
        roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        XCTAssertEqual(game.score(), 0)
    }

    func testNoStrikesOrSpares() {
        roll([3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6, 3, 6])
        XCTAssertEqual(game.score(), 90)
    }

    func testSpareFollowedByZeros() {
        roll([6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        XCTAssertEqual(game.score(), 10)
    }

    func testPointsScoredInRollAfterSpare() {
        roll([6, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        XCTAssertEqual(game.score(), 16)
    }

    func testConsecutiveSpares() {
        roll([5, 5, 3, 7, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        XCTAssertEqual(game.score(), 31)
    }

    func testSpareInLastFrame() {
        roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 3, 7])
        XCTAssertEqual(game.score(), 17)
    }

    func testSpareWithTwoRollBonus() {
        roll([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 7, 3])
        XCTAssertEqual(game.score(), 20)
    }

    func testPerfectGame() throws {
        try roll(knockPins: 10, times: 12)
        XCTAssertEqual(game.score(), 300)
    }
}
